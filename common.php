<?php
	if($include_config=="a7ada8UReBepaqaMaMYGAGeqatA6abYbUBu3YRaZy4ySYLEHe9s"){
		function getip() {
			if (isSet($_SERVER)) {
				if (isSet($_SERVER["HTTP_X_FORWARDED_FOR"])){
					$realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
				}elseif(isSet($_SERVER["HTTP_CLIENT_IP"])) {
					$realip = $_SERVER["HTTP_CLIENT_IP"];
				}else{
					$realip = $_SERVER["REMOTE_ADDR"];
				}
			} else {
				if(getenv( 'HTTP_X_FORWARDED_FOR')){
					$realip = getenv('HTTP_X_FORWARDED_FOR');
				}elseif( getenv('HTTP_CLIENT_IP')){
					$realip = getenv('HTTP_CLIENT_IP');
				}else{
					$realip = getenv('REMOTE_ADDR');
				}
			}
			return($realip);		
		}
	}	
?>