<?php
	if($include_config=="a7ada8UReBepaqaMaMYGAGeqatA6abYbUBu3YRaZy4ySYLEHe9s"){
		
		function returnUserType($sessionDataIdentifier,$session_user_type_visitor_id,$tablesArray,$expirationTime,$loadUserTypeFolder){
			$sessionId=$_SESSION[$sessionDataIdentifier];
			$userTypeId="0";
			$sessionArray=dbSelect($tablesArray['USER_SESSION'],array("user_id","ip","lasttime"),array(array("status_id","1"),array("user_session_id",$sessionId)));
			
			if(count($sessionArray)!=0){
				//SESSION ACTIVE, CHECK IP
				if($sessionArray[0]['ip']==getip()){
					//Ultima hora que no pase
					$db_data = strtotime($sessionArray[0]['lasttime']);
					date_default_timezone_set("UTC");
					$now=time();
					$expir_mili=$expirationTime*60*60;
					if($db_data+$expir_mili>$now){
						$usersArray=dbSelect("users",array("role"),array(array("role","1"),array("id",$sessionArray[0]['user_id'])));
						if(count($usersArray)!=0){
							$userTypeId=$usersArray[0]['role'];
							$updates=dbUpdate($tablesArray['USER_SESSION'],array(array("lasttime","UTC_TIMESTAMP()")),array(array("status_id","1"),array("user_id",$sessionArray[0]['user_id'])));
						}
					}else{
						//EXPIRATION TIME 	
						unset($_SESSION[$sessionDataIdentifier]);
						$updates=dbUpdate($tablesArray['USER_SESSION'],array(array("status_id","2"),array("lasttime","UTC_TIMESTAMP()")),array(array("status_id","1"),array("user_id",$sessionArray[0]['user_id'])));	
					}
				}else{
					//DIFFERENT IP CLOSE SESSION
					unset($_SESSION[$sessionDataIdentifier]);
					$updates=dbUpdate($tablesArray['USER_SESSION'],array(array("status_id","2"),array("lasttime","UTC_TIMESTAMP()")),array(array("status_id","1"),array("user_id",$sessionArray[0]['user_id'])));	
				}
			}
			return($userTypeId);
		}
	}
?>