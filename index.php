<?php
	//SESSION METHODS
	session_start();
	
	//DB VARIABLES
	$connection=NULL;
	$db=NULL;
	$dbVisitorKey="YRaZy4ySa7ada8UReeqatA6abBepaqaMaMYGAGYbUBu3YLEHe9s";
	$tablesArray=array();
	
	//INCLUDE KEY
	$include_config="a7ada8UReBepaqaMaMYGAGeqatA6abYbUBu3YRaZy4ySYLEHe9s";
	
	//INCLUDES
	include("config.php");
	include("db.php");
	include("common.php");
	
	//LOAD VARIABLES
	$loadUserTypeFolder=VISITOR_FOLDER;
	$loadModuleFolder=VISITOR_HOME_MODULE;
	$userType=SESSION_USER_TYPE_VISITOR_ID;
	
	//READ ADDRESS
	$path  = $_GET['path'];
	$urlArray = explode("/", $path);
	
	//LOGOUT
	if(isset($_SESSION[SESSION_DATA])==true){
		dbVisitorConnect($dbVisitorKey);
		$sessionId=$_SESSION[SESSION_DATA];
		if($urlArray[0]=="cerrar"){
			$sessionArray=dbSelect($tablesArray['USER_SESSION'],array("user_id"),array(array("status_id","1"),array("user_session_id",$sessionId)));
			$updates=dbUpdate($tablesArray['USER_SESSION'],array(array("status_id","2"),array("lasttime","UTC_TIMESTAMP()")),array(array("status_id","1"),array("user_id",$sessionArray[0]['user_id'])));
			unset($_SESSION[SESSION_DATA]);
		}
		disconnect();
	}
	//READ SESSION
	if(isset($_SESSION[SESSION_DATA])==true){
		//CHECK IF THE SESSION IS A VALID ONE
		include("session.php");
		dbVisitorConnect($dbVisitorKey);
		$userType=returnUserType(SESSION_DATA,SESSION_USER_TYPE_VISITOR_ID,$tablesArray,SESSION_EXPIRATION_TIME,$loadUserTypeFolder);
		if($userType=="1"){
			$userType="2";
			disconnect();
			//CREATE NEW CONNECTION ACCORDING TO USERTYPE
			$loadUserTypeFolder="admin";
			$key="4ySaGAyYYRaZYbUBu3Ge9saMaM7ada8UReeqatA6abBepaqYLEH";
			dbAdminConnect($key);	
		}
	}else{
		dbVisitorConnect($dbVisitorKey);
		$userType="1";
	}
	$userTypeFolder=dbSelect($tablesArray['USER_TYPE'],array("folder"),array(array("status_id","1"),array("user_type_id",$userType)));
	if(count($userTypeFolder)!=0){
		$loadUserTypeFolder=$userTypeFolder[0]['folder'];
	}
	
	
	if(isset($urlArray[0])==true && $urlArray[0]!=""){
		//SELECT PRIMARY MODULE
		$module=dbSelect($tablesArray['MODULE'],array("folder"),array(array("status_id","1"),array("user_type_id",$userType),array("primary","true")));
		if(count($module)!=0){
			$loadModuleFolder=$module[0]['folder'];
		}
		//SELECT IDENTIFIER MODULE
		$temp_identifier=$urlArray[0];
		$module=dbSelect($tablesArray['MODULE'],array("folder"),array(array("status_id","1"),array("user_type_id",$userType),array("identifier",$temp_identifier)));
		if(count($module)!=0){
			$loadModuleFolder=$module[0]['folder'];
		}
	}else{
		//SELECT PRIMARY MODULE
		$module=dbSelect($tablesArray['MODULE'],array("folder"),array(array("status_id","1"),array("user_type_id",$userType),array("primary","true")));
		if(count($module)!=0){
			$loadModuleFolder=$module[0]['folder'];
		}
	}
	//INCLUDE
	include($loadUserTypeFolder."/modules/".$loadModuleFolder."/index.php");
	disconnect();
	

?>