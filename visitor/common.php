<?php
if($include_config=="a7ada8UReBepaqaMaMYGAGeqatA6abYbUBu3YRaZy4ySYLEHe9s"){
	function randPass($length, $strength=8) {
		$vowels = 'aeuy';
		$consonants = 'bdghjmnpqrstvz';
		if ($strength >= 1) {
			$consonants .= 'BDGHJLMNPQRSTVWXZ';
		}
		if ($strength >= 2) {
			$vowels .= "AEUY";
		}
		if ($strength >= 4) {
			$consonants .= '23456789';
		}
		if ($strength >= 8) {
			$consonants .= 'FGH';
		}
	
		$password = '';
		$alt = time() % 2;
		for ($i = 0; $i < $length; $i++) {
			if ($alt == 1) {
				$password .= $consonants[(rand() % strlen($consonants))];
				$alt = 0;
			} else {
				$password .= $vowels[(rand() % strlen($vowels))];
				$alt = 1;
			}
		}
		return $password;
	}
	
	function getip() {
		if (isSet($_SERVER)) {
			if (isSet($_SERVER["HTTP_X_FORWARDED_FOR"])){
  				$realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
 			}elseif(isSet($_SERVER["HTTP_CLIENT_IP"])) {
  				$realip = $_SERVER["HTTP_CLIENT_IP"];
 			}else{
  				$realip = $_SERVER["REMOTE_ADDR"];
 			}
		} else {
 			if(getenv( 'HTTP_X_FORWARDED_FOR')){
  				$realip = getenv('HTTP_X_FORWARDED_FOR');
 			}elseif( getenv('HTTP_CLIENT_IP')){
 				$realip = getenv('HTTP_CLIENT_IP');
 			}else{
  				$realip = getenv('REMOTE_ADDR');
 			}
		}
		return($realip);		
	}
	
		function getRealIpAddr()
		{
			if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
			{
			  $ip=$_SERVER['HTTP_CLIENT_IP'];
			}
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
			{
			  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			else
			{
			  $ip=$_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		}
	}
?>