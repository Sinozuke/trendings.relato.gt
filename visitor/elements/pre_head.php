<?php 
	if($include_config=="a7ada8UReBepaqaMaMYGAGeqatA6abYbUBu3YRaZy4ySYLEHe9s"){
		//Set de caracteres
		echo("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
		
		//Favicon
		//echo("<link rel=\"icon\" href=\"".INSTALLATION_HOST.$loadUserTypeFolder."images/favicon.ico\" type=\"image/x-icon\"/>");
		
		//Pantallas en mobiles que se mantengan sin zoom
		echo("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1.0, user-scalable=no\">");
		
		//Cargar fonts de google
		echo("<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i' rel='stylesheet' type='text/css'>");
		echo("<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>");
		

		//Librerias en Javascript
		echo("<script type=\"text/javascript\" src=\"".INSTALLATION_URL.$loadUserTypeFolder."/libs/js/jquery.js\"></script>");
		echo("<script type=\"text/javascript\" src=\"".INSTALLATION_URL.$loadUserTypeFolder."/libs/js/jqueryui.js\"></script>");
		echo("<script type=\"text/javascript\" src=\"".INSTALLATION_URL.$loadUserTypeFolder."/libs/js/bootstrap.js\"></script>");
		echo("<script type=\"text/javascript\" src=\"".INSTALLATION_URL.$loadUserTypeFolder."/libs/js/DD_roundies.js\"></script>");

		//CSS General
		echo("<link href=\"".INSTALLATION_URL.$loadUserTypeFolder."/style.css\" rel=\"stylesheet\" type=\"text/css\">");
		
		//SWIPER
		echo("<link href=\"".INSTALLATION_URL.$loadUserTypeFolder."/libs/swiper/dist/css/swiper.css\" rel=\"stylesheet\" type=\"text/css\">"); 
		echo("<script type=\"text/javascript\"src=\"".INSTALLATION_URL.$loadUserTypeFolder."/libs/swiper/dist/js/swiper.min.js\"></script>");
	
		
		echo("<script type=\"text/javascript\" src=\"".INSTALLATION_URL.$loadUserTypeFolder."/modules/".$loadModuleFolder."/actions.js\"></script>");
		
		//Common Custom
		echo("<script type=\"text/javascript\" src=\"".INSTALLATION_URL.$loadUserTypeFolder."/variables.js\"></script>");  
		echo("<script type=\"text/javascript\" src=\"".INSTALLATION_URL.$loadUserTypeFolder."/functions.js\"></script>");  
		echo("<script type=\"text/javascript\" src=\"".INSTALLATION_URL.$loadUserTypeFolder."/common.js\"></script>"); 	
			
	}
?>