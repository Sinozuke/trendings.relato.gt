<?php 
	if($config_include_key=="aMYGAGeaZy4ySYLa7ada8URqatA6abYbUBu3YReBepaqaMEHe9"){
		//Set de caracteres
		echo("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
		
		//Favicon
		//echo("<link rel=\"icon\" href=\"".$hostname.$website_folder."images/favicon.ico\" type=\"image/x-icon\"/>");
		
		//Pantallas en mobiles que se mantengan sin zoom
		echo("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1.0, user-scalable=no\">");
		
		//Cargar fonts de google
		echo("<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i' rel='stylesheet' type='text/css'>");
		echo("<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>");
		
		
	
		
		//Librerias en Javascript
		echo("<script type=\"text/javascript\" src=\"".$hostname.$website_folder."libs/js/jquery.js\"></script>");
		echo("<script type=\"text/javascript\" src=\"".$hostname.$website_folder."libs/js/jqueryui.js\"></script>");
		echo("<script type=\"text/javascript\" src=\"".$hostname.$website_folder."libs/js/bootstrap.js\"></script>");
		echo("<script type=\"text/javascript\" src=\"".$hostname.$website_folder."libs/js/DD_roundies.js\"></script>");
	
		
		//Custom Libraries
		
		//CSS General
		echo("<link href=\"".$hostname.$website_folder."common.css\" rel=\"stylesheet\" type=\"text/css\">");
		
		
		//CSS Header 
		/*echo("<link href=\"".$hostname.$website_folder."elements/header/style.css\" rel=\"stylesheet\" type=\"text/css\">");
		echo("<link href=\"".$hostname.$website_folder."elements/footer/style.css\" rel=\"stylesheet\" type=\"text/css\">");*/
		/*	
		//CSS Footer 
		/*echo("<link href=\"".$var_site_url.$var_site_website_foldername."themes/".$var_site_theme."elements/footer.css\" rel=\"stylesheet\" type=\"text/css\">");
		*/
		//CUSTOM LIBRARIES
		/*echo("<script type=\"text/javascript\" src=\"".$hostname.$website_folder."libs/js/DD_roundies.js\"></script>");*/
		//Swiper
		//echo("<link rel=\"stylesheet\" href=\"".$var_site_url.$var_site_foldername.$var_site_website_foldername."libs/swiper/dist/css/swiper.min.css\">");
		echo("<link href=\"".$hostname.$website_folder."libs/swiper/dist/css/swiper.css\" rel=\"stylesheet\" type=\"text/css\">"); 
		echo("<script type=\"text/javascript\"src=\"".$hostname.$website_folder."libs/swiper/dist/js/swiper.min.js\"></script>");
	
		
		//Common Custom
		echo("<script type=\"text/javascript\" src=\"".$hostname.$website_folder."variables.js\"></script>");  
		echo("<script type=\"text/javascript\" src=\"".$hostname.$website_folder."functions.js\"></script>");  
		echo("<script type=\"text/javascript\" src=\"".$hostname.$website_folder."common.js\"></script>"); 	
		
		//Elements
		echo("<script type=\"text/javascript\" src=\"".$hostname.$website_folder."elements/u_header/actions.js\"></script>"); 	
		echo("<script type=\"text/javascript\" src=\"".$hostname.$website_folder."elements/footer/actions.js\"></script>"); 
			
	}
?>