function textf_paint_clean(target){
	iconurl=hostname+website_folder+"images/false.png"
	target.css("background-image","none");
}
function textf_paint_check(target){
	iconurl=hostname+website_folder+"images/true.png"
	target.css("background-image","url('"+iconurl+"')");
}
function textf_paint_cross(target){
	iconurl=hostname+website_folder+"images/false.png"
	target.css("background-image","url('"+iconurl+"')");
}


function recalculate_sizes(){
	//FONTS & SIZES
	for(var i=0;i<f_numsizes.length;i++){
		var num=f_numsizes[i];
		f_sizes[num]['current']=num;
		if(stage_width<screen_breakpoint){
			tam=calculate_size(f_sizes[num]['min'],f_sizes[num]['max'],rel_ancho);
			if(tam<11){
				tam=11;
			}
			f_sizes[num]['current']=tam;
		}
		
		$(".f"+num).css("font-size",f_sizes[num]['current']+"px");
	}
}
function init_sizes(){
	for(var i=0;i<f_numsizes.length;i++){
		var num=f_numsizes[i];
		f_sizes[num]=new Array();
		f_sizes[num]['max']=num;
		f_sizes[num]['min']=num*min_relative_size;
		if(num<12){
			num=12;
		}
		f_sizes[num]['current']=num;
	}
}


function repaint_common(){
	stage_width=$(window).innerWidth();
	stage_height=$(window).innerHeight();
	outter_width=stage_width;
	if(stage_width>=screen_breakpoint){	
		rel_ancho=1;
	}else{
		rel_ancho=stage_width/screen_breakpoint;
	}
	recalculate_sizes();
	
	$('.f50_height').height(Math.round(f_sizes[50]['current']));
	$('.f30_height').height(Math.round(f_sizes[30]['current']));
	$('.f20_height').height(Math.round(f_sizes[20]['current']));
	
	
	
	module_repaint();
}
function paint_init(){
	stage_width=$(window).innerWidth();
	stage_height=$(window).innerHeight();
	if(stage_width>=screen_breakpoint){	
		rel_ancho=1;
	}else{
		rel_ancho=stage_width/screen_breakpoint;
	}
	init_sizes();
}
jQuery(function($){
	paint_init();
});

$(document).ready(function() {
	paint_init();
	//init_elements();
	//header_init();
	//footer_init();
	
	$(window).unbind("resize");
	$(window).resize(function () {			
		repaint_common();
	});
	$(window).unbind("scroll");
	$(window).scroll(function () {		
		//header_scroll();
		repaint_common();
	});
	
	
	$( 'html' ).animate({ opacity: 1}, {duration: 'fast',specialEasing: { opacity: "easeInOutQuint",  }, complete: function() {} });
	
	
	
	
	module_init();
	repaint_common();
});

window.onbeforeunload = function(){
	
}