background_ori_width=200;
background_ori_height=115;
background_ori_first_height=70;
maxtf_size=330;
max_banner_width=392;
max_banner_size=381;

banner_width=max_banner_width;
//Messages
message_name_error1="Ingrese un correo electrónico.";
message_name_error2="Ingrese un correo electrónico válido.";
message_name_error3="Ingrese una contraseña válida";
message_name_error4="Correo electrónico no existente.";
message_name_error5="Contraseña incorrecta.";
message_name_error6="Servidor ocupado, por favor intente enviar su correo más tarde.";

function mensaje(type,message){
	//alert(message);
	switch(type){
		case "error":
			$('#message').html(message);
			$('#message_area').show('fast',function(){
				module_repaint();
			});
		break;	
	}	
}
function ingresoPass(){
	valor1=$("#ingreso_tf1").val();
	valor2=$("#ingreso_tf2").val();
	//Realizar Envío

	phpurl=window_origin+"/"+userTypeFolder+"/modules/"+moduleFolder+"/login.php";

	$.post(phpurl,{
		p1:valor1,
		p2:valor2
		}, function(data){
			switch(data){
				case "ok":
					window.location.href=window_origin;
				break;
				case "email":
					mensaje("error",message_name_error4);
				break;
				case "pass":
					mensaje("error",message_name_error5);
				break;
				case "error2":
					mensaje("error",message_name_error6);
				break;
				case "error1":
					mensaje("error",message_name_error6);
				break;
				default:
					mensaje("error",data);
				break;
			}
	});
}
function ingresoFormEval(){
	campo=$("#ingreso_tf1");
	valor=campo.val();
	if(valor==""){
		mensaje("error",message_name_error1);
	}else{
		if(valor.length<2){
			mensaje("error",message_name_error2);
		}else{
			if(validateEmail(valor)==true){
				campo=$("#ingreso_tf2");
				valor=campo.val();
				if(valor.length>5){
					ingresoPass();	
				}else{
					mensaje("error",message_name_error3);
				}
			}else{
				mensaje("error",message_name_error2);
			}
		}
	}
}
function adminIngresar(){
	ingresoFormEval();
}
function module_repaint(){
	
	$('#visitor_content').width(stage_width);
	$('#visitor_background').width(stage_width);
	if(stage_width>=screen_breakpoint){	
		vback=(stage_height-(stage_width/background_ori_width*background_ori_first_height));
		$('#visitor_background').css("top",vback+"px");
		$('.centered_content_inside').width(screen_breakpoint-2*f_sizes[50]['current']-f_sizes[10]['current']);
		
		$('.tf').width(maxtf_size);
		$('#message_area').width(maxtf_size+f_sizes[50]['current']-f_sizes[10]['current']);
		
		banner_width=max_banner_width;
	}else{
		$('#visitor_background').css("top",(stage_height-(stage_width/background_ori_width*background_ori_first_height))+"px");
		$('.centered_content_inside').width(stage_width-2*f_sizes[50]['current']);
		size_tf=stage_width-2*(f_sizes[20]['current'])-37-44;
		if(size_tf>maxtf_size){
			$('.tf').width(maxtf_size);
			$('#message_area').width(maxtf_size+4*f_sizes[12]['current']-3);
			
			banner_width=maxtf_size;
		}else{
			$('.tf').width(size_tf);
			$('#message_area').width(size_tf+4*f_sizes[12]['current']-3);
			
			banner_width=size_tf;
		}
		
	}
	
	$('.visitor_table_header_title').css("padding-left",(f_sizes[8]['current'])+"px");
	$('.visitor_table_header_title').css("padding-right",(f_sizes[8]['current'])+"px");
	
	$('.visitor_table_header_title2').css("padding-left","0px");
	$('.visitor_table_header_title2').css("padding-right",(f_sizes[12]['current'])+"px");
	
	$('.login_margined1').css("margin-left",(f_sizes[40]['current'])+"px");
	$('.login_margined2').css("margin-left",(f_sizes[20]['current'])+"px");
	
	$('.tf').css("padding-top",f_sizes[12]['current']+"px");
	$('.tf').css("padding-bottom",f_sizes[12]['current']+"px");
	$('.tf').css("padding-right","40px");
	
	
	$('.lrmargined_login').css("margin-left",f_sizes[20]['current']+"px");
	$('.lrmargined_login').css("margin-right",f_sizes[20]['current']+"px");
	
	$('.boton_ingresar').css("padding-top",f_sizes[12]['current']+"px");
	$('.boton_ingresar').css("padding-bottom",f_sizes[12]['current']+"px");
	$('.alert_icon').css("left",f_sizes[12]['current']+"px");
	$('#message').css("padding",f_sizes[12]['current']+"px");
	$('#message2').css("padding",f_sizes[12]['current']+"px");
	
	$('.banner_width').width(banner_width);
}
function module_init(){
	module_repaint();
}